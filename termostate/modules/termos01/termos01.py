import RPi.GPIO as GPIO
import smbus
import time
import math
address = 0x48
bus=smbus.SMBus(1)
cmd=0x40

class Termos:
    def analogRead(chn):
        value = bus.read_byte_data(address,cmd+chn)
        return value
    def analogWrite(value):
        bus.write_byte_data(address,cmd,value)
    def setup():
        GPIO.setmode(GPIO.BOARD)
    def loop():
        value = Termos.analogRead(0)
        #read A0 pin
        voltage = value / 255.0 * 3.3
        #calculate voltage
        Rt = 10 * voltage / (3.3 - voltage) #calculate resistance value of thermistor
        tempK = 1/(1/(273.15 + 25) + math.log(Rt/10)/3950.0) #calculate temperature(Kelvin)
        tempC = tempK -273.15

        #calculate temperature (Celsius)
        print('ADC Value : %d, Voltage : %.2f, Temperature : %.2f'%(value,voltage,tempC))
        time.sleep(0.01)
        return {"names": [{"value": [value]}, {"voltage": [voltage]}, {"temperature": [tempC]}]}

    def destroy():
        GPIO.cleanup()   
def run():
    try:
        Termos.setup()
        data = Termos.loop()
        Termos.destroy()
        return data
    except KeyboardInterrupt:
        Termos.destroy()
    finally:
        Termos.destroy()
