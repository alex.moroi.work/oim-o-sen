from flask import Flask, jsonify
from modules.termos01.termos01 import *
termos = Termos()

app = Flask(__name__)


@app.route('/')
def index():
    return 'Flask is running!'


@app.route('/data')
def names():
    data = {"names": ["John", "Jacob", "Julie", "Jennifer"]}
    return jsonify(data)

@app.route('/termos')
def termos_01():
    data = run()
    return jsonify(data)

if __name__ == '__main__':
    app.run(host = '0.0.0.0' ,port='4000')

